FROM python:3.9

# создаем директорию
WORKDIR /code

# для прода должно быть очень мало либ, только самые нужные
RUN pip install --upgrade pip
RUN pip install poetry

# копируем файлы
COPY ./pyproject.toml /code/
COPY ./inference.py /code/app/inference.py
COPY ./.env /code/app/.env

# апускаем поетри установку зависимостей
RUN poetry config virtualenvs.create false \
    && poetry install $(test "$YOUR_ENV" == production && echo "--no-dev") --no-interaction --no-ansi

# запускаем сервер ювикорн приложение
CMD ["uvicorn", "app.inference:app", "--host", "0.0.0.0", "--port", "80"]
