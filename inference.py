import pandas as pd
import mlflow
import os
from dotenv import load_dotenv
import uvicorn
from fastapi import FastAPI, File, UploadFile, HTTPException
import pandas as pd
import warnings

warnings.simplefilter(action="ignore", category=pd.errors.PerformanceWarning)

load_dotenv()

app = FastAPI()

model_name = "churn_prediction"
stage = "Staging"
version = "0.1.0."


class Model:
    """
    Model loading from registry
    Predict incoming data
    """

    def __init__(self, model_name, model_stage):
        self.model = mlflow.pyfunc.load_model(f"models:/{model_name}/{model_stage}")

    def predict(self, data):
        preds = self.model.predict(data)
        return preds.tolist()


model = Model(model_name, stage)


@app.get("/info")
async def model_info():
    """Return model information, version, how to call"""
    return {"name": model_name, "version": version}


@app.get("/health")
async def service_health():
    """Return service health"""
    return {"ok"}


@app.post("/predict")
async def create_upload_file(file: UploadFile = File(...)):
    """Return predicted values or error"""
    if file.filename.endswith(".csv"):
        with open(file.filename, "wb") as f:
            f.write(file.file.read())
        data = pd.read_csv(file.filename)
        os.remove(file.filename)
        classes = model.predict(data)
        return classes

    else:
        raise HTTPException(
            status_code=400, detail="Invalid file format. Only CSV Files accepted"
        )


if os.getenv("AWS_ACCESS_KEY_ID") is None or os.getenv("AWS_SECRET_ACCESS_KEY") is None:
    exit(1)
